#!/bin/bash
echo 'Logging as fr user'
echo '===================== DEPLOYING HAPROXY ====================='
echo 'Installing Dependencies ....'
sudo yum install gcc pcre-static pcre-devel -y
echo 'Installation Completed ...'
echo 'Deploying HAPROXY ...'
cd /root/idam
sudo tar xzvf $software_dir/haproxy-1.6.6.tar.gz
sudo mv /root/idam/haproxy-1.6.6 $haproxy_install_dir
echo $pwd "path of $haproxy_install_dir"
sudo chown -R fr:fr $haproxy_install_dir/*
sudo chown -R fr:fr $haproxy_install_dir/.gitignore
sudo chmod -R g+rw $haproxy_install_dir/*
cd $haproxy_install_dir
sudo make TARGET=linux2628
sudo make install
echo 'HAPROXY Deployed ...'
echo 'Copying Settings ...'
sudo cp /usr/local/sbin/haproxy /usr/sbin/
sudo cp $haproxy_install_dir/examples/haproxy.init /etc/init.d/haproxy
sudo chmod 755 /etc/init.d/haproxy
echo 'Settings Copied ...'
echo 'Creating Directories ...'
sudo mkdir -p /etc/haproxy
sudo mkdir -p /run/haproxy
sudo mkdir -p /var/lib/haproxy
sudo touch /var/lib/haproxy/stats
echo 'Directories Created ...'
echo 'Creating haproxy user ...'
sudo useradd -r haproxy
echo 'haproxy user Created ...'
echo 'Confirming HAPROXY Version ...'
haproxy -v
echo 'Confirmed ...'
echo 'HAPROXY Deployment Completed!'
cd $base_path 
