#!/bin/bash
echo '===================== CONFIGURING HAPROXY ====================='
echo 'Creating Configuration File for Load Balancing between OpenIDM and OpenDJ 1 ...' 
sudo echo '#LDAP HAProxy Configuration File for Dev-Local Environment
defaults
 log global
 option dontlognull
 timeout connect 500ms
 timeout client 30s
 timeout server 30s
frontend openidm
 bind 127.0.0.1:666
 default_backend opendj
backend opendj
 balance roundrobin
 server opendj1 127.0.0.1:1389 check' \
> $haproxy_config_file
sudo chown -R fr:fr $haproxy_config_file
sudo chmod -R g+rw $haproxy_config_file
echo 'Editing LDAP Connector ...'
sudo sed -i 's/1389/666/g' /opt/forgerock/openidm/idm1/conf/provisioner.openicf-opendj.json
echo 'LDAP Connector Edited ... HAPROXY Listenning on Port 666 and Forwarding Traffic to OpenDJ1 on Port 1389 ...'
echo 'Starting HAProxy Service ...'
sudo service haproxy start

